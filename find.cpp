#include <iostream>
#include <unistd.h>
#include <string>
#include <filesystem>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;
using namespace filesystem;
using namespace algorithm;

void filecheck(string path, string find, char** argv, int argc) {

    if (path.rfind("/proc", 0) == -1) {

        int access(const char *path, int mode);
        char c[path.size() + 1];
        strcpy(c, path.c_str());

    if(access(c, R_OK) == 0) {
        for (const auto & entry : directory_iterator(path)) {
        if(!is_symlink(entry)) {
        string entrylocation(entry.path().string());

        if(is_directory(entry)) {
                filecheck(entry.path(), find, argv, argc);
                if (ends_with(entrylocation, find)) {
                cout << "FOUND: " << entrylocation << "\n";
                }
            }
        if(is_regular_file(entry)) {
            if (ends_with(entrylocation, find)) {
                cout << "FOUND: " << entrylocation << "\n";
                }
        }
            } 
    }
    } else {
        string verbosity("false");
        if(argc == 4) {
            string verbosity(argv[3]);
        if(algorithm::starts_with(verbosity, "-v")) {
        cout << c << ": Permission denied" << "\n";
        }
        }
    }
    }
}


int main(int argc, char** argv) {

    if(argc == 1) {
        cout << "Please enter a path to search" << "\n";
        exit(127);
    }

    if(argc == 2) {
        cout << "Please enter an item to search" << "\n";
        exit(127);
    }

    string path(argv[1]);
    string find(argv[2]);

    
    filecheck(path, find, argv, argc);
}


