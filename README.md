# better-find

A better find command meant for linux, but can easily be ported to other operating systems.

# syntax

bfind [start directory] [item to search for] [-verbose]
